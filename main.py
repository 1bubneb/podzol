import os
import subprocess
import curses
import json

import gui
import config

def main():
    config.loadServers()
    curses.wrapper(gui.mainWindow)

main()
