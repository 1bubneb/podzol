import curses
import os

import config

class Point(object):
    def __init__(self, y,x):
        self.x = x
        self.y = y

class Cursor(object):
    def __init__(self, y, x, char):
        Point.__init__(self, y, x)
        self.char = char
        self.lastChar = ' '

    def move(self, stdscr, direction, bounds):
        stdscr.addstr(self.y, self.x, self.lastChar)
        if (direction == curses.KEY_RIGHT) or (direction == curses.KEY_DOWN):
            self.increment(stdscr, direction)
        else:
            self.decrement(stdscr, direction)
        self.draw(stdscr)

    def draw(self, stdscr):
        self.lastChar = chr(stdscr.inch(self.y, self.x) & 0xFF)
        stdscr.addstr(self.y, self.x, self.char)
        stdscr.refresh()

    def increment(self, stdscr, direction, bounds):
        if self.x < bounds.x and direction == curses.KEY_RIGHT:
            self.x += 1
        if self.y < bounds.y and direction == curses.KEY_DOWN:
            self.y +=1

    def decrement(self, stdscr, direction):
        if self.x > 0 and direction == curses.KEY_LEFT:
            self.x -= 1
        if self.y > 0 and direction == curses.KEY_UP:
            self.y -= 1

class Menu(object):
    def __init__(self, y, x, items, handler):
        self.initialY = y
        self.initialX = x
        self.items = items
        self.len = len(items)
        self.maxY = self.initialY + self.len - 1
        self.cursor = Cursor(y, x, '*')
        self.handler = handler

    def draw(self, stdscr, bloop):
        j=0
        try:
            for i in range(0, len(items)):
                stdscr.addstr(self.initialY + j, self.initialX, bloop + self.items[i])
                j = j + 1
            return 0
            stdscr.refresh()
        except:
            return 1

    def input(self, stdscr):
        uInput = stdscr.getch()
        if uInput == curses.KEY_UP:
            self.cursor.move(stdscr, uInput)
            return 0
        if uInput == curses.KEY_DOWN:
            self.cursor.move(stdscr, uInput)
            return 0
        if uInput == curses.KEY_LEFT:
            self.cursor.move(stdscr, uInput)
            return 0
        if uInput == curses.KEY_RIGHT:
            self.cursor.move(stdscr, uInput)
            return 0
        if uInput == ord('q'):
            return 1

def mainWindow(stdscr):
    drawTitlebar(stdscr, "podzol server manager")
    #listdirFormatted(stdscr, 2, 4, "./")
    #listServers(stdscr, 2, 4, config.SERVERS)
    stdscr.nodelay(1)

    things = ["item 1", "item 2", "item 3"]

    menu = Menu(2, 4, things, inputHandler)

    menu.draw(stdscr, '-')

    #print(stdscr.inch(0,0) & 0xFF)

def inputHandler(stdscr, uInput):
    while inputReturn != 1:
        stdscr.refresh()
        uInput = stdscr.getch()
        inputReturn = inputHandler(stdscr, uInput)

def listServers(stdscr, startingY, x, servers):
    j=0
    try:
        for i in servers:
            stdscr.addstr(startingY + j, x, "- " + servers[i].name + ", " + servers[i].version)
            j = j + 1
        return 0
    except:
        return 1

def listdirFormatted(stdscr, startingY, x, path):
    directories = os.listdir(path)
    j = 0
    try:
        for i in directories:
            stdscr.addstr(startingY + j, x, "- " + i)
            j = j + 1
        return 0
    except:
        return 1

def getBounds(stdscr):
    temp = stdscr.getmaxyx()
    bounds = Point(temp[0], temp[1])

    return bounds

def drawTitlebar(stdscr, title):
    bounds = getBounds(stdscr)
    center = Point(int(bounds.y / 2), int(bounds.x / 2))

    stdscr.addstr(0, int((center.x) - (len(title) / 2)),  title)

    for i in range(0, int((center.x) - (len(title) / 2))):
        stdscr.addstr(0, i, "=", curses.A_REVERSE)

    for i in range(int((center.x) - (len(title) / 2) + len(title)), bounds.x):
        stdscr.addstr(0, i, "=", curses.A_REVERSE)

    stdscr.refresh()
    return 0
