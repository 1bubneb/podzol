import os
import json

SERVERS = {}

config = json.load(open('./config.json'))

class server(object):
    def __init__(self, name, memory, version, worldDir, args):
        self.name = name
        self.version = version
        self.memory = memory
        self.worldDir = worldDir
        self.args = args

def loadServers():
    for i in os.listdir(config["dirs"]["serversroot"]):
        for j in os.listdir(config["dirs"]["serversroot"] + "/" + i):
            if j == "server-conf.json":
                serverConf = json.load(open(config["dirs"]["serversroot"] + "/" + i + "/server-conf.json"))
                SERVERS[i] = server(i, serverConf["memory"], serverConf["version"], serverConf["worldDir"], serverConf["args"])
